/*
 * Copyright (C) 2023 Cobblemon Contributors
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.cobblemon.mod.common.world.predicate

import com.cobblemon.mod.common.platform.PlatformRegistry
import net.minecraft.registry.Registries
import net.minecraft.registry.Registry
import net.minecraft.registry.RegistryKey
import net.minecraft.registry.RegistryKeys
import net.minecraft.world.gen.blockpredicate.BlockPredicateType

object CobblemonBlockPredicates: PlatformRegistry<Registry<BlockPredicateType<*>>, RegistryKey<Registry<BlockPredicateType<*>>>, BlockPredicateType<*>>() {

    override val registry: Registry<BlockPredicateType<*>> = Registries.BLOCK_PREDICATE_TYPE
    override val registryKey: RegistryKey<Registry<BlockPredicateType<*>>> = RegistryKeys.BLOCK_PREDICATE_TYPE

    @JvmField
    val ALTITUDE = create("altitude", BlockPredicateType { AltitudePredicate.CODEC })

//    fun <P : BlockPredicate?> register(id: String, codec: Codec<P>): BlockPredicateType<P> {
//        return Registry.register(Registries.BLOCK_PREDICATE_TYPE, cobblemonResource(id), BlockPredicateType { codec })
//    }

//    fun touch() = Unit
}