/*
 * Copyright (C) 2023 Cobblemon Contributors
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.cobblemon.mod.common.client.render.models.blockbench.pokemon.ms0

import com.cobblemon.mod.common.client.render.models.blockbench.PoseableEntityState
import com.cobblemon.mod.common.client.render.models.blockbench.asTransformed
import com.cobblemon.mod.common.client.render.models.blockbench.pokemon.PokemonPose
import com.cobblemon.mod.common.client.render.models.blockbench.pokemon.PokemonPoseableModel
import com.cobblemon.mod.common.entity.PoseType
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import net.minecraft.client.model.ModelPart
import net.minecraft.util.math.Vec3d

class OrangemushroomModel(root: ModelPart) : PokemonPoseableModel() {

    override val rootPart = root.registerChildWithAllChildren("orangemushroom")

    val right_eye = getPart("right_eye")
    val left_eye = getPart("left_eye")
    val b_eye_0 = getPart("b_eye_0")
    val b_eye_1 = getPart("b_eye_1")
    val b_eye_2 = getPart("b_eye_2")
    val b_eye_3 = getPart("b_eye_3")

    override val portraitScale = 1.25F
    override val portraitTranslation = Vec3d(0.0, -1.3, 0.0)

    override val profileScale = 0.8F
    override val profileTranslation = Vec3d(0.0, 0.04, 0.0)

    lateinit var battleStanding : PokemonPose
    lateinit var standing: PokemonPose
    lateinit var walk: PokemonPose

    override fun registerPoses() {
        val blink = quirk("blink") { bedrockStateful("orangemushroom", "blink").setPreventsIdle(false) }

        battleStanding = registerPose(
            poseName = "battleStanding",
            transformTicks = 10,
            poseTypes = PoseType.STATIONARY_POSES,
            condition = { it.isBattling },
            transformedParts = arrayOf(
                b_eye_0.asTransformed().withVisibility(visibility = true),
                b_eye_1.asTransformed().withVisibility(visibility = true),
                b_eye_2.asTransformed().withVisibility(visibility = true),
                b_eye_3.asTransformed().withVisibility(visibility = true),
                right_eye.asTransformed().withVisibility(visibility = false),
                left_eye.asTransformed().withVisibility(visibility = false)
            ),
            idleAnimations = arrayOf(
                bedrock("orangemushroom", "battle_idle")
            )
        )

        standing = registerPose(
            poseName = "standing",
            poseTypes = PoseType.STATIONARY_POSES + PoseType.UI_POSES,
            transformTicks = 10,
            quirks = arrayOf(blink),
            condition = { !it.isBattling },
            transformedParts = arrayOf(
                b_eye_0.asTransformed().withVisibility(visibility = false),
                b_eye_1.asTransformed().withVisibility(visibility = false),
                b_eye_2.asTransformed().withVisibility(visibility = false),
                b_eye_3.asTransformed().withVisibility(visibility = false),
                right_eye.asTransformed().withVisibility(visibility = true),
                left_eye.asTransformed().withVisibility(visibility = true)
            ),
            idleAnimations = arrayOf(
                bedrock("orangemushroom", "ground_idle")
            )
        )

        walk = registerPose(
            poseName = "walk",
            poseTypes = PoseType.MOVING_POSES,
            transformTicks = 10,
            quirks = arrayOf(blink),
            condition = { !it.isBattling },
            transformedParts = arrayOf(
                b_eye_0.asTransformed().withVisibility(visibility = false),
                b_eye_1.asTransformed().withVisibility(visibility = false),
                b_eye_2.asTransformed().withVisibility(visibility = false),
                b_eye_3.asTransformed().withVisibility(visibility = false),
                right_eye.asTransformed().withVisibility(visibility = true),
                left_eye.asTransformed().withVisibility(visibility = true)
            ),
            idleAnimations = arrayOf(
                bedrock("orangemushroom", "ground_walk")
            )
        )
    }

    override fun getFaintAnimation(
        pokemonEntity: PokemonEntity,
        state: PoseableEntityState<PokemonEntity>
    ) = if (state.isPosedIn(battleStanding)) bedrockStateful("orangemushroom", "faint") else null
}